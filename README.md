# Absol
<a href="https://gitmoji.dev">
  <img src="https://img.shields.io/badge/gitmoji-%20😜%20😍-FFDD67.svg?style=flat-square" alt="Gitmoji">
</a>

yet another unfinished PokeWalker emulator

## building absol
run cmake

## TODOs
everything

## why make another one
In short, because I want to.
It's to challenge myself.
And by making my own I can have a codebase that motivates me to work.
And even if I'm not directly contributing to another emulators codebase, anything I archive will still help them progress.

## Credits
This would not be possible without the prior work of some awesome people that I continue to reference.
- DmitryGR and their [blog](http://dmitry.gr/?r=05.Projects&proj=28.%20pokewalker)
- UnrealPowerz and their [powar emulator](https://github.com/UnrealPowerz/powar)
- happylappy, PoroCYon and more for being awesome

## License
licensed under The Unlicense, so do what you want.
While not required, please do give credit where you can.

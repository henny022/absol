cmake_minimum_required(VERSION 3.23)
project(absol)

set(CMAKE_CXX_STANDARD 23)

find_package(SDL2 REQUIRED)

add_subdirectory(src)

#include "gui.h"

#include <SDL.h>

absol::gui::Context::Context(int width, int height)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        fputs("error initializing sdl", stderr);
        fputs(SDL_GetError(), stderr);
        exit(1);
    }
    window = SDL_CreateWindow(
            "absol",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            width, height,
            SDL_WINDOW_SHOWN
    );
    if (!window)
    {
        fputs("error creating window", stderr);
        fputs(SDL_GetError(), stderr);
        exit(1);
    }
}

absol::gui::Context::~Context()
{
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void absol::gui::Context::frame(absol::emulator::State &state)
{
    SDL_Event e;
    while (SDL_PollEvent(&e))
    {
        switch (e.type)
        {
            case SDL_QUIT:
                state.running = false;
                break;
            default:
                break;
        }
    }
}

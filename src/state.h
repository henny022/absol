#ifndef ABSOL_STATE_H
#define ABSOL_STATE_H

namespace absol::emulator
{
struct State
{
    bool running = true;
};
}

#endif //ABSOL_STATE_H

#ifndef ABSOL_GUI_H
#define ABSOL_GUI_H

#include "state.h"

struct SDL_Window;

namespace absol::gui
{
struct Context
{
    Context(int width, int height);
    ~Context();
    Context(const Context &) = delete;
    Context(Context &&) = delete;
    Context &operator=(const Context &) = delete;
    Context &operator=(Context &&) = delete;

    void frame(emulator::State &state);

    SDL_Window *window;
};
}

#endif //ABSOL_GUI_H

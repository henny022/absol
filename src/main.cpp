#include <SDL.h>
#include "gui.h"

int main()
{
    absol::gui::Context gui(96 * 5, 64 * 5);
    absol::emulator::State emu;

    while (emu.running)
    {
        gui.frame(emu);
    }
}
